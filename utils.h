#ifndef UTILS_H
#define UTILS_H

#include <mutex>
#include <condition_variable>
#include <atomic>
#include <queue>
#include <thread>
#include <functional>

template<typename T>
class thread_safe_queue
{
    std::mutex mtx_;
    std::condition_variable cond_;
    std::queue<T> q_;
    std::atomic<bool> stop_{false};

public:
    void push(T item)
    {
        {
            std::lock_guard<std::mutex> l(mtx_);
            q_.push(std::move(item));
        }
        cond_.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> l(mtx_);
        while(q_.empty() && !stop_) cond_.wait(l);
        if(stop_) return;
        item = std::move(q_.front());
        q_.pop();
    }

    void reset()
    {
        std::lock_guard<std::mutex> l(mtx_);
        stop_ = false;
        while (!q_.empty()) q_.pop();
    }

    void stop()
    {
        stop_ = true;
        cond_.notify_all();
    }

    bool is_stoped()
    {
        return stop_;
    }
};

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<std::thread> thds;

public:
    thread_pool(int n_of_threads)
    {
        for (int i = 0 ; i < n_of_threads ; ++i)
        {
            thds.emplace_back([this]
                {
                    for(;;)
                    {
                        task_t task;
                        q.pop(task);
                        if(!task) return;
                        std::cout << "going to execute task " << std::this_thread::get_id() << std::endl;
                        task();
                    }
                }
            );
        }
    }

    void submit(task_t task)
    {
        if(task)
            q.push(task);
    }

    ~thread_pool()
    {
        for (int i = 0 ; i < thds.size() ; ++i)
            q.push(task_t());
        for (auto& th : thds) th.join();
    }
};

#endif // UTILS_H

