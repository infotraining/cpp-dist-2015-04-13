#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>
#include <atomic>
#include <mutex>

using namespace std;

timed_mutex mtx;

void worker(int id)
{
    cout << "starting " << id << endl;
    unique_lock<timed_mutex> ul(mtx, try_to_lock);
    if(!ul.owns_lock())
    {
        cout << id << " didn't get lock" << endl;
        while(!ul.try_lock_for(500ms))
        {
            cout << id << " waiting" << endl;
        }
    }
    cout << id << " got lock " << endl;
    this_thread::sleep_for(5s);
}

int main()
{
    thread th1(worker, 1);
    thread th2(worker, 2);
    th1.join();
    th2.join();
    return 0;
}

