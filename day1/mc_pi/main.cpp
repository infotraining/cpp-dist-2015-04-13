#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>

using namespace std;

void get_counter(long N, long& counter)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dist(-1, 1);

    for (long i = 0 ; i < N ; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);
        if (x*x + y*y < 1) ++counter;
    }
}

int main()
{
    auto start = chrono::high_resolution_clock::now();
    long N = 10000000;
    long counter{};
    get_counter(N, counter);
    cout << "Pi = " << double(counter)/double(N)*4 << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed " << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;


    int n_of_threads = thread::hardware_concurrency();

    vector<thread> thds;
    thds.reserve(n_of_threads);

    vector<long> counters(n_of_threads);

    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < n_of_threads ; ++i)
    {
        thds.emplace_back(get_counter, N/n_of_threads, ref(counters[i]));
    }

    for(auto &th : thds) th.join();

    cout << "Pi = " << accumulate(counters.begin(), counters.end(), 0.0)/double(N)*4 << endl;

    end = chrono::high_resolution_clock::now();
    cout << "Elapsed " << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    return 0;
}

