#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <list>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        {
            lock_guard<mutex> lg(mtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    int msg;
    for(;;)
    {
        {
            unique_lock<mutex> lg(mtx);
            while(q.empty())
            {
                cond.wait(lg);
            }
            msg = q.front();
            q.pop();
            cout << id << " got " << msg << endl;
        }
    }
}


int main()
{
    thread thp(producer);
    thread thc1(consumer, 1);
    thread thc2(consumer, 2);
    thp.join();
    thc1.join();
    thc2.join();
    return 0;
}

