#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

long counter{};
atomic<long> at_counter{0};
mutex mtx;

void increment()
{
    for (int i = 0 ; i < 100000 ; ++i)
        ++counter;
}

void at_increment()
{
    for (int i = 0 ; i < 100000 ; ++i)
        at_counter.fetch_add(1);
}

void mtx_increment()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        ++counter;
        if (counter == 10000) return;
    }
}

int main()
{
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increment);
    for (auto& th :thds)
        th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;

    //***************************

    cout << "Really atomic? " << at_counter.is_lock_free() << endl;

    thds.clear();
    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(at_increment);
    for (auto& th :thds)
        th.join();

    end = chrono::high_resolution_clock::now();
    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << at_counter << endl;

    //***************************

    counter = 0;
    thds.clear();
    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(mtx_increment);
    for (auto& th :thds)
        th.join();

    end = chrono::high_resolution_clock::now();
    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;

    return 0;
}

