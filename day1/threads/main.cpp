#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello world" << endl;
}

void say_id(int id)
{
    cout << "hello from " << id << endl;
}

void increment(int& a)
{
    a++;
}

struct F
{
    void operator()()
    {
        cout << "functor" << endl;
    }
};

thread gen_thread()
{
    int a{42};
    return thread([a] { cout << "Got a = " << a << endl;});
}

int main()
{
    thread th1(hello);
    th1.join();
    thread th2(say_id, 10);
    th2.join();
    int a = 10;
    thread th3(increment, ref(a));
    th3.join();
    cout << "a = " << a << endl;
    thread th4{F{}};
    th4.join();
    thread th5( [&] { cout << "Lambda " << a << endl;});
    th5.join();
    thread th6(gen_thread());
    thread th7 = move(th6);
    cout << "th7 is a thread? " << th7.joinable() << endl;
    cout << "th6 is a thread? " << th6.joinable() << endl;
    th7.join();

    vector<thread> threads;
    threads.push_back(thread( [] { cout << "L1" << endl;}));
    threads.emplace_back(hello);

    for (auto& th : threads) th.join();
    return 0;
}

