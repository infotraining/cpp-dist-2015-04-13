#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>
#include <thread>

using namespace std;
using namespace AmqpClient;

string ex_input = "input";
string q_input = "q_input";
string ex_output = "primes";
string q_output = "q_primes";

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");
    channel->DeclareExchange(ex_input, Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareExchange(ex_output, Channel::EXCHANGE_TYPE_DIRECT);

    channel->DeclareQueue(q_input, false, false, false, false);
    channel->BindQueue(q_input, ex_input);

    channel->DeclareQueue(q_output, false, false, false, false);
    channel->BindQueue(q_output, ex_output);
}

void producer()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");

    for (int i = 1000000 ; i < 1000100 ; ++i)
    {
        channel->BasicPublish(ex_input, "", BasicMessage::Create(to_string(i)));
    }
}

void printer()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");
    channel->BasicConsume(q_output,"", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    setup();
    cout << "after setup" << endl;
    thread th1(producer);
    thread th2(printer);
    th1.join();
    th2.join();
    return 0;
}

