#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>

using namespace std;

int main()
{
    cout << "Hello zmq server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);

    socket.connect("tcp://188.166.119.100:6666");

    for(int i = 0 ; i < 5 ; ++i)
    {
        s_send(socket, "msg from client - Leszek");
        cout << "just got " << s_recv(socket) << endl;
    }
    return 0;
}


