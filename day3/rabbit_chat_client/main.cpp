#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>
#include <thread>

using namespace std;
using namespace AmqpClient;

string ex_name = "chat_input";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672,
                              "admin", "tymczasowe");

    for(;;)
    {
        string msg;
        cout << ">>> ";
        getline(cin, msg);
        msg = "[Leszek] " + msg;
        channel->BasicPublish(ex_name, "",
                          BasicMessage::Create(msg));
    }
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672,
                              "admin", "tymczasowe");

    string q_name = channel->DeclareQueue("");
    channel->BindQueue(q_name, ex_name, "");
    channel->BasicConsume(q_name);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Simple AMQP chat client" << endl;
    thread th_send(send);
    thread th_receive(receive);
    th_send.join();
    th_receive.join();

    return 0;
}
