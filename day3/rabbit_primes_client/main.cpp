#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>
#include <thread>

using namespace std;
using namespace AmqpClient;

string ex_input = "input";
string q_input = "q_input";
string ex_output = "primes";
string q_output = "q_primes";

bool is_prime(int n)
{
    for (int div = 2 ; div < n ; ++div )
        if (div % n == 0) return false;
    return true;
}

void worker()
{
    Channel::ptr_t ch_inp;
    ch_inp = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");

    ch_inp->BasicConsume(q_input,"", true, false, false, 1);
    for(;;)
    {
        Envelope::ptr_t env = ch_inp->BasicConsumeMessage();
        int val = stoi( env->Message()->Body());
        string pri = " not prime ";
        if (is_prime(val))
        {
            pri = " prime";
        }
        cout << "got: " << val << pri << endl;
        ch_inp->BasicPublish(ex_output, "", BasicMessage::Create(
                                 "Leszek: "s + to_string(val) + pri));
        ch_inp->BasicAck(env);
    }
}

int main()
{
    cout << "Hello World!" << endl;
    worker();
    return 0;
}

