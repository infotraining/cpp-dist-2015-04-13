#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello zmq fwd" << endl;
    zmq::context_t context(1);
    zmq::socket_t soc_in(context, ZMQ_PULL);
    zmq::socket_t soc_out(context, ZMQ_PUSH);

    soc_in.bind("tcp://*:9999");
    soc_out.bind("tcp://*:9910");

    for(;;)
    {
        //cout << "got " << s_recv(soc_in) << endl;
        s_send(soc_out, s_recv(soc_in));
    }
    return 0;
}


