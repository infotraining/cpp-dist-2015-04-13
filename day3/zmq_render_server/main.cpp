#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello zmq render server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_ROUTER);

    socket.bind("tcp://*:8888");

    for(;;)
    {
        string tmp;
        getline(cin, tmp);
        for(int i = 0 ; i < 768; ++i)
        {
            // handshake
            string address = s_recv(socket);
            s_recv(socket);
            string id = s_recv(socket);
            cout << "req from " << address << " " << id << endl;
            // sending
            s_sendmore(socket, address);
            s_sendmore(socket, "");
            s_send(socket, to_string(i) + " 64");
        }
    }
    return 0;
}


