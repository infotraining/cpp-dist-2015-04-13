#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <list>
#include <condition_variable>
#include "../../utils.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 0 ; i < 20 ; ++i)
    {
        q.push(i);
        this_thread::sleep_for(50ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        if(q.is_stoped()) return;
        cout << id << " got " << msg << endl;
    }
}

int main()
{
    thread thp(producer);
    thread thc1(consumer, 1);
    thread thc2(consumer, 2);
    thp.join();
    q.stop();
    thc1.join();
    thc2.join();
    return 0;
}

