#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <list>
#include <condition_variable>
#include "../../utils.h"
#include <future>
#include <numeric>

using namespace std;

template<typename It, typename T>
future<T> parallel_accumulate(It begin, It end, T init)
{
    auto size = distance(begin, end);
    int n_of_threads = 4;
    auto block_size = size/n_of_threads;

    It block_start = begin;
    vector<shared_future<T>> results;

    for (int i = 0 ; i < n_of_threads ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if(i == n_of_threads-1) block_end = end;
        results.push_back( async(launch::async,
                           accumulate<It, T>,
                           block_start,
                           block_end,
                           T()));
        block_start = block_end;
    }

    return async(launch::deferred, [results, init] () mutable {
                 T res{init};
                 for( auto& fut : results)
                     res += fut.get();
                 return res;
    });
}

int main()
{
    vector<double> v;
    for (int i = 0 ; i < 10000000 ; ++i)
        v.push_back(i);
    cout << "sum = " << accumulate(v.begin(), v.end(), 0L) << endl;
    //cout << "sum = " << parallel_accumulate(v.begin(), v.end(), 0L) << endl;
    future<long> res = parallel_accumulate(v.begin(), v.end(), 0L);
    cout << "sum = " << res.get() << endl;
    return 0;
}

