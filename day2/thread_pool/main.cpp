#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <list>
#include <condition_variable>
#include "../../utils.h"
#include <functional>

using namespace std;

void hello()
{
    cout << "Hello fun" << endl;
}

struct F
{
    void operator()()
    {
         cout << "Hello F" << endl;
    }
};

int main()
{
    hello();
    F f;
    f();
    auto l = + [] {  cout << "hello lambda" << endl; };
    l();

    function<void()> fun;
    fun = hello;
    if (fun)
    {
        cout << "fun ready" << endl;
        fun();
    }

    thread_safe_queue<function<void()>> q;
    q.push(f);
    q.pop(fun);
    fun();

    thread_pool tp(4); // threads creation
    tp.submit(f);      // queue feeding
    for(int i = 0 ; i < 50 ; ++i)
        tp.submit( [i] {
            cout << "Lambda " << i << endl;
            this_thread::sleep_for(200ms);
        });
    cout << "All tasks send";
    return 0;
}

