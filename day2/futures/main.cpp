#include <iostream>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <list>
#include <condition_variable>
#include "../../utils.h"
#include <future>

using namespace std;

int question(int id)
{
    this_thread::sleep_for(100ms);
    if (id == 13) throw std::logic_error("bad luck");
    return 42;
}

int main()
{
    future<int> ans = async(launch::async, question, 13);
    ans.wait();
//    cout << "result is ready" << endl;
//    cout << "result = " << ans.get() << endl;

    try
    {
//        thread th1(question,13);
//        th1.join();  // couldn't catch exception
        cout << "result = " << ans.get() << endl;
    }
    catch(std::logic_error& err)
    {
        cerr << "error: " << err.what() << endl;
    }

    return 0;
}

