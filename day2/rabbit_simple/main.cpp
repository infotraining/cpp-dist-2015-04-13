#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>
#include <thread>

using namespace std;
using namespace AmqpClient;

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");
    channel->DeclareExchange("input", Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareQueue("q", false, false, false, false);
    channel->BindQueue("q","input");
}

void test()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");
    channel->BasicPublish("input", "", BasicMessage::Create("hello world"));
}

void get()
{
    Channel::ptr_t channel;
    channel = Channel::Create("188.166.119.100", 5672, "admin", "tymczasowe");
    channel->BasicConsume("q","", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    setup();
    test();
    get();
    return 0;
}

